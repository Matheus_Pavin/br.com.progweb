package br.com.progweb.objects;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;

public class Imovel extends IdImovel{

	private Integer quantidadeQuarto;

	private Integer quantidadeBanheiro;

	private Integer quantidadeGaragem;

	private boolean possuiMobilia;

	private String intensAdicionais;

	private Double valorVenda;
	

	public Integer getQuantidadeQuarto() {
		return quantidadeQuarto;
	}

	public void setQuantidadeQuarto(Integer quantidadeQuarto) {
		this.quantidadeQuarto = quantidadeQuarto;
	}

	public Integer getQuantidadeBanheiro() {
		return quantidadeBanheiro;
	}

	public void setQuantidadeBanheiro(Integer quantidadeBanheiro) {
		this.quantidadeBanheiro = quantidadeBanheiro;
	}

	public Integer getQuantidadeGaragem() {
		return quantidadeGaragem;
	}

	public void setQuantidadeGaragem(Integer quantidadeGaragem) {
		this.quantidadeGaragem = quantidadeGaragem;
	}

	public boolean getPossuiMobilia() {
		return possuiMobilia;
	}

	public void setPossuiMobilia(boolean possuiMobilia) {
		this.possuiMobilia = possuiMobilia;
	}

	public String getIntensAdicionais() {
		return intensAdicionais;
	}

	public void setIntensAdicionais(String intensAdicionais) {
		this.intensAdicionais = intensAdicionais;
	}

	public Double getValorVenda() {
		return valorVenda;
	}

	public void setValorVenda(Double valorVenda) {
		this.valorVenda = valorVenda;
	}
	
	public BasicDBObject setDbObject() {
		BasicDBObject document = new BasicDBObject();
		document.put("quantidadeQuarto", getQuantidadeQuarto());
		document.put("quantidadeBanheiro", getQuantidadeBanheiro());
		document.put("quantidadeGaragem", getQuantidadeGaragem());
		document.put("possuiMobilia", getPossuiMobilia());
		document.put("itensAdicionais", getIntensAdicionais());
		document.put("valorVenda", getValorVenda());
		document.put("idImovel", getIdImovel());
		document.put("tipo", getTipo());
		String teste = (new Gson().toJson(getCliente()));
		document.put("cliente", teste);
		document.put("elevador", null);
		document.put("esquina", null);
		document.put("tamanho", null);
		return document;
	}

	public Imovel getDbObject(BasicDBObject dbObject) {
		Imovel imovel = new Imovel();
		imovel.setQuantidadeQuarto(dbObject.getInt("quantidadeQuarto"));
		imovel.setQuantidadeBanheiro(dbObject.getInt("quantidadeBanheiro"));
		imovel.setQuantidadeGaragem(dbObject.getInt("quantidadeGaragem"));
		imovel.setPossuiMobilia(dbObject.getBoolean("possuiMobilia"));
		imovel.setIntensAdicionais(dbObject.getString("itensMobilia"));
		imovel.setValorVenda(dbObject.getDouble("valorVenda"));
		imovel.setIdImovel(dbObject.getString("idImovel"));
		imovel.setTipo(dbObject.getString("idImovel"));
		imovel.setCliente((Cliente) dbObject.get("cliente")); //duvida cm pavin
		
		
		return imovel;
	}

}
