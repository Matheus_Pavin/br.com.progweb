package br.com.progweb.objects;

import com.mongodb.BasicDBObject;

public class Login {

	private String nome;

	private String senha;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public BasicDBObject setDbObject() {
		BasicDBObject document = new BasicDBObject();
		document.put("nome", getNome());
		document.put("senha", getSenha());
		return document;
	}

	public Login getDbObject(BasicDBObject dbObject) {
		Login login = new Login();
		login.setNome(dbObject.getString("nome"));
		login.setSenha(dbObject.getString("senha"));
		return login;
	}

}
