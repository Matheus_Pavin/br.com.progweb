package br.com.progweb.objects;

import com.mongodb.BasicDBObject;

public class Apartamento extends Imovel{
	
	private boolean elevador;
	
	private Integer andar;

	public boolean getElevador() {
		return elevador;
	}

	public void setElevador(boolean elevador) {
		this.elevador = elevador;
	}
	
	public Integer getAndar() {
		return andar;
	}

	public void setAndar(Integer andar) {
		this.andar = andar;
	}

	public BasicDBObject setDbObject(Imovel imovel) {
		BasicDBObject document = imovel.setDbObject();
		document.put("elevador", getElevador());
		document.put("andar", getAndar());
		document.put("esquina", null);
		document.put("tamanho", null);

		return document;
	}

	public Apartamento getDbObject(BasicDBObject dbObject) {
		Apartamento Apartamento = new Apartamento();
		Apartamento.setElevador(dbObject.getBoolean("elevador"));
		
		return Apartamento;
	}

}
