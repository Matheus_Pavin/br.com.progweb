package br.com.progweb.objects;

import java.io.Serializable;
import java.util.UUID;

import com.mongodb.BasicDBObject;

public class Cliente implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7731275552433104785L;

	private String idCliente;

	private String cpf;

	private String dataNascimento;

	private String nome;

	public String getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	
	public void setIdCliente() {
		this.idCliente = UUID.randomUUID().toString();
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BasicDBObject setDbObject() {
		BasicDBObject document = new BasicDBObject();
		document.put("id", getIdCliente());
		document.put("cpf", getCpf());
		document.put("dataNascimento", getDataNascimento());
		document.put("nome", getNome());
		return document;
	}

	public Cliente getDbObject(BasicDBObject dbObject) {
		Cliente cliente = new Cliente();
		cliente.setIdCliente(dbObject.getString("id"));
		cliente.setCpf(dbObject.getString("cpf"));
		cliente.setDataNascimento("dataNascimento");
		cliente.setNome(dbObject.getString("nome"));
		return cliente;
	}
	
	

}
