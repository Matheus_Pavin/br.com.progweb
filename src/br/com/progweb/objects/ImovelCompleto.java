package br.com.progweb.objects;

public class ImovelCompleto {
	
	private Integer quantidadeQuarto;
	
	private String idImovel;

	private Integer quantidadeBanheiro;

	private Integer quantidadeGaragem;

	private boolean possuiMobilia;

	private String intensAdicionais;

	private Double valorVenda;
	
	private boolean esquina;

	private String tamanho;

	private String tipo;

	private Cliente cliente;
	
	private boolean elevador;
	
	private Integer andar;

	public Integer getQuantidadeQuarto() {
		return quantidadeQuarto;
	}

	public void setQuantidadeQuarto(Integer quantidadeQuarto) {
		this.quantidadeQuarto = quantidadeQuarto;
	}

	public String getIdImovel() {
		return idImovel;
	}

	public void setIdImovel(String idImovel) {
		this.idImovel = idImovel;
	}

	public Integer getQuantidadeBanheiro() {
		return quantidadeBanheiro;
	}

	public void setQuantidadeBanheiro(Integer quantidadeBanheiro) {
		this.quantidadeBanheiro = quantidadeBanheiro;
	}

	public Integer getQuantidadeGaragem() {
		return quantidadeGaragem;
	}

	public void setQuantidadeGaragem(Integer quantidadeGaragem) {
		this.quantidadeGaragem = quantidadeGaragem;
	}

	public boolean isPossuiMobilia() {
		return possuiMobilia;
	}

	public void setPossuiMobilia(boolean possuiMobilia) {
		this.possuiMobilia = possuiMobilia;
	}

	public String getIntensAdicionais() {
		return intensAdicionais;
	}

	public void setIntensAdicionais(String intensAdicionais) {
		this.intensAdicionais = intensAdicionais;
	}

	public Double getValorVenda() {
		return valorVenda;
	}

	public void setValorVenda(Double valorVenda) {
		this.valorVenda = valorVenda;
	}

	public boolean isEsquina() {
		return esquina;
	}

	public void setEsquina(boolean esquina) {
		this.esquina = esquina;
	}

	public String getTamanho() {
		return tamanho;
	}

	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public boolean isElevador() {
		return elevador;
	}

	public void setElevador(boolean elevador) {
		this.elevador = elevador;
	}

	public Integer getAndar() {
		return andar;
	}

	public void setAndar(Integer andar) {
		this.andar = andar;
	}
	

}
