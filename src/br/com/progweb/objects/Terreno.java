package br.com.progweb.objects;

import com.mongodb.BasicDBObject;

public class Terreno extends IdImovel {

	private boolean esquina;

	private String tamanho;

	private Double valorVenda;

	public boolean getEsquina() {
		return esquina;
	}

	public void setEsquina(boolean esquina) {
		this.esquina = esquina;
	}

	public String getTamanho() {
		return tamanho;
	}

	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}

	public Double getValorVenda() {
		return valorVenda;
	}

	public void setValorVenda(Double valorVenda) {
		this.valorVenda = valorVenda;
	}

	public BasicDBObject setDbObject(Imovel imovel) {
		BasicDBObject document = imovel.setDbObject();
		document.put("elevador", null);
		document.put("esquina", getEsquina());
		document.put("tamanho", getTamanho());

		return document;
	}

	public Terreno getDbObject(BasicDBObject dbObject) {
		Terreno terreno = new Terreno();
		terreno.setEsquina(dbObject.getBoolean("esquina"));
		terreno.setTamanho(dbObject.getString("tamanho"));

		return terreno;
	}
}
