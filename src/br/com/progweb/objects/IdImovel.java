package br.com.progweb.objects;

import java.util.UUID;

public class IdImovel {

	private String idImovel;

	private String tipo;

	private Cliente cliente;

	public String getIdImovel() {
		return idImovel;
	}

	public void setIdImovel() {
		this.idImovel = UUID.randomUUID().toString();
	}

	public void setIdImovel(String idImovel) {
		this.idImovel = idImovel;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}
