package br.com.progweb.factory;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;

import br.com.progweb.objects.Cliente;

public class MongoFactory {

	DB db;

	public MongoFactory() {
	}

	public void insertMongo(BasicDBObject documentObject, String table) throws MongoException, UnknownHostException {
		getConnection();
		DBCollection collection = db.getCollection(table);
		BasicDBObject insertDocument = createObjectMongo(table, documentObject);
		collection.insert(insertDocument);
	}

	public List<DBObject> searchMongo(BasicDBObject documentObject, String table)
			throws MongoException, UnknownHostException {

		List<DBObject> listObjects = new ArrayList<DBObject>();
		getConnection();
		DBCollection collection = db.getCollection(table);
		try {
			BasicDBObject searchQuery = new BasicDBObject();
			DBCursor cursor = collection.find(searchQuery);
			while (cursor.hasNext()) {
				listObjects.add(cursor.next());
				System.out.println(cursor.next());
			}

		} catch (Exception e) {

		}
		return listObjects;
	}

	public List<BasicDBObject> searchMongoNotComplete(BasicDBObject whereQuery, String table)
			throws MongoException, UnknownHostException {
		List<BasicDBObject> listBasicDBObject = new ArrayList<BasicDBObject>();
		getConnection();
		DBCollection collection = db.getCollection(table);
		try {
			DBCursor cursor = collection.find(whereQuery);
			while (cursor.hasNext()) {
				DBObject object = cursor.next();
				boolean is = object.containsKey("detail");
				listBasicDBObject.add((BasicDBObject) object.get("detail"));
			}

		} catch (Exception e) {

		}

		return listBasicDBObject;
	}

	public void deleteMongo(BasicDBObject documentObject, String table) {
		getConnection();
		DBCollection collection = db.getCollection(table);
		collection.remove(documentObject);
		collection.drop();
	}

	public void changeMongo(BasicDBObject documentObjectOld, BasicDBObject documentObjectNew, String table)
			throws MongoException, UnknownHostException {
		List<DBObject> listDbSearch = searchMongo(documentObjectOld, table);

		if (listDbSearch.size() > 1)
			throw new MongoException("Erro na localização");

		getConnection();
		DBCollection collection = db.getCollection(table);
		BasicDBObject searchQuery = documentObjectOld;
		collection.update(searchQuery, documentObjectNew);


	}

	private BasicDBObject createObjectMongo(String table, BasicDBObject documentObject) {
		BasicDBObject document = new BasicDBObject();
		document.put("database", "progweb");
		document.put("table", table);

		document.put("detail", documentObject);

		return document;

	}

	private void getConnection() {

		try {
			MongoClient mongoClient = new MongoClient("127.0.0.1");
			this.db = mongoClient.getDB("progweb");
		} catch (MongoException | UnknownHostException e) {
		}
	}

}
