package br.com.progweb.factory;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.progweb.objects.Apartamento;
import br.com.progweb.objects.Casa;
import br.com.progweb.objects.Imovel;
import br.com.progweb.objects.ImovelCompleto;
import br.com.progweb.objects.Terreno;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;

public class CadastraImovel extends HttpServlet {

	Mapper mapper = new Mapper();
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CadastraImovel() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		MongoFactory mongoFactory = new MongoFactory();
		String imovelString = request.getParameter("imovel");
		JsonParser parser = new JsonParser();
		BasicDBObject query = new BasicDBObject();
		if (imovelString != null) {
			JsonElement jArray = parser.parse(imovelString).getAsJsonObject();
			JsonObject obj = parser.parse(imovelString).getAsJsonObject();
			query.put("detail.tipo", obj.get("tipo").getAsString());
		}

		List<ImovelCompleto> listResponseBD = mapper.imovelMapper(mongoFactory
				.searchMongoNotComplete(query, "imovel"));
		if (listResponseBD.size() == 0)
			throw new IOException("Não existem imóveis cadastrados.");

		String json = new Gson().toJson(listResponseBD);
		response.setContentType("application/json");
		response.getWriter().write(json);

	}

	/**
	 * 
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		MongoFactory mongoFactory = new MongoFactory();
		String imovelString = request.getParameter("imovel");
		Terreno terreno = new Terreno();
		Apartamento apartamento = new Apartamento();
		Imovel imovel = new Imovel();
		Casa casa = new Casa();
		BasicDBObject basicObj = new BasicDBObject();

		JsonParser parser = new JsonParser();
		JsonElement jArray = parser.parse(imovelString).getAsJsonObject();
		JsonObject obj = parser.parse(imovelString).getAsJsonObject();
		if (obj.get("tipo").getAsString().equalsIgnoreCase("terreno")) {
			imovel = new Gson().fromJson(jArray, Imovel.class);
			imovel.setIdImovel();
			terreno.setTamanho(obj.get("tamanho").getAsString());
			terreno.setEsquina(obj.get("esquina").getAsBoolean());
			basicObj = terreno.setDbObject(imovel);
			terreno = new Gson().fromJson(jArray, Terreno.class);
			terreno.setIdImovel();
			basicObj = terreno.setDbObject(imovel);
		} else if (obj.get("tipo").getAsString()
				.equalsIgnoreCase("apartamento")) {
			imovel = new Gson().fromJson(jArray, Imovel.class);
			imovel.setIdImovel();
			String elevador = obj.get("elevador").getAsString();
			if (elevador.equalsIgnoreCase("true")) {
				apartamento.setElevador(true);
			}else{
				apartamento.setElevador(false);
			}
			apartamento.setElevador(obj.get("elevador").getAsBoolean());
			apartamento.setAndar(obj.get("andar").getAsInt());
			basicObj = apartamento.setDbObject(imovel);

		} else if (obj.get("tipo").getAsString().equalsIgnoreCase("casa")) {
			casa = new Gson().fromJson(jArray, Casa.class);
			casa.setIdImovel();

			basicObj = casa.setDbObject();
		} else {
			throw new IOException(
					"Não foi possível encontrar classe de imóvel compatível");
		}

		mongoFactory.insertMongo(basicObj, "imovel");

	}
}
