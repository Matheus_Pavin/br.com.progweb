package br.com.progweb.factory;

import java.util.ArrayList;
import java.util.List;

import br.com.progweb.objects.Cliente;
import br.com.progweb.objects.ImovelCompleto;
import br.com.progweb.objects.Login;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;

public class Mapper {

	public List<Cliente> clienteMapper(List<BasicDBObject> listBasicDBObject) {

		List<Cliente> listClientes = new ArrayList<Cliente>();
		for (BasicDBObject dbObject : listBasicDBObject) {
			String nome = dbObject.getString("nome");
			String dataNascimento = dbObject.getString("dataNascimento");
			String cpf = dbObject.getString("cpf");
			String idCliente = dbObject.getString("idCliente");

			Cliente cliente = new Cliente();
			cliente.setNome(nome);
			cliente.setCpf(cpf);
			cliente.setDataNascimento(dataNascimento);
			cliente.setIdCliente(idCliente);

			listClientes.add(cliente);
		}

		return listClientes;
	}

	public List<Login> loginMapper(List<BasicDBObject> listBasicDBObject) {

		List<Login> listLogin = new ArrayList<Login>();
		for (BasicDBObject dbObject : listBasicDBObject) {
			String nome = dbObject.getString("nome");
			String senha = dbObject.getString("senha");

			Login login = new Login();
			login.setNome(nome);
			login.setSenha(senha);

			listLogin.add(login);
		}

		return listLogin;
	}

	public List<ImovelCompleto> imovelMapper(
			List<BasicDBObject> listBasicDBObject) {
		Cliente cliente = new Cliente();
		List<ImovelCompleto> listImovel = new ArrayList<ImovelCompleto>();
		for (BasicDBObject dbObject : listBasicDBObject) {

			String idImovel = dbObject.getString("idImovel");
			Integer quantidadeQuarto = (Integer) dbObject
					.get("quantidadeQuarto");
			Integer quantidadeBanheiro = (Integer) dbObject
					.get("quantidadeBanheiro");
			Integer quantidadeGaragem = (Integer) dbObject
					.get("quantidadeGaragem");
			boolean possuiMobilia = (boolean) dbObject.get("possuiMobilia");
			String intensAdicionais = (String) dbObject.get("intensAdicionais");
			Double valorVenda = (Double) dbObject.get("valorVenda");
			String tipo = (String) dbObject.get("tipo");
			JsonParser parser = new JsonParser();
			String clienteString = (String) dbObject.get("cliente");
			if (!clienteString.equalsIgnoreCase("null")) {
				JsonElement jArray = parser.parse(
						(String) dbObject.get("cliente")).getAsJsonObject();
				cliente = new Gson().fromJson(jArray, Cliente.class);
			}

			ImovelCompleto imovel = new ImovelCompleto();

			if (!tipo.equalsIgnoreCase("null")) {
				if (tipo.equalsIgnoreCase("terreno")) {
					boolean esquina = (boolean) dbObject.get("esquina");
					String tamanho = (String) dbObject.get("tamanho");
					imovel.setEsquina(esquina);
					imovel.setTamanho(tamanho);

				} else if (tipo.equalsIgnoreCase("apartamento")) {
					Integer andar = (Integer) dbObject.get("andar");
					boolean elevador = (boolean) dbObject.get("elevador");
					imovel.setAndar(andar);
					imovel.setElevador(elevador);
				}
			}

			imovel.setCliente(cliente);
			imovel.setIdImovel(idImovel);
			imovel.setIntensAdicionais(intensAdicionais);
			imovel.setPossuiMobilia(possuiMobilia);
			imovel.setQuantidadeBanheiro(quantidadeBanheiro);
			imovel.setQuantidadeGaragem(quantidadeGaragem);
			imovel.setQuantidadeQuarto(quantidadeQuarto);
			imovel.setTipo(tipo);
			imovel.setValorVenda(valorVenda);

			listImovel.add(imovel);
		}

		return listImovel;
	}
}
