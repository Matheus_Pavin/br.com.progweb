package br.com.progweb.factory;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.progweb.objects.Cliente;
import br.com.progweb.objects.Login;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;

public class CadastraLogin extends HttpServlet {

	MongoFactory mongoFactory = new MongoFactory();
	Mapper mapper = new Mapper();
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CadastraLogin() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		BasicDBObject query = new BasicDBObject();
		String loginString = request.getParameter("login");
		JsonParser parser = new JsonParser();
		if (loginString == null)
			throw new IOException("Erro");
		JsonElement jArray = parser.parse(loginString).getAsJsonObject();
		Login login = new Gson().fromJson(jArray, Login.class);
		query.put("detail.nome", login.getNome());
		List<Login> listResponseBD = mapper.loginMapper(mongoFactory
				.searchMongoNotComplete(null, "login"));

		if (listResponseBD.size() == 0) {
			throw new IOException("Nome Incorreto");
		} else if (listResponseBD.size() > 1) {
			throw new IOException("Nome Incorreto");

		} else if (!listResponseBD.get(0).getSenha()
				.equalsIgnoreCase(login.getSenha())) {
			throw new IOException("Senha Incorreta");
		} else {

			response.addHeader("login", "true");
			response.sendRedirect("/br.com.progweb/listaTodos.html");
			return;
//			String json = new Gson().toJson(listResponseBD);
//			response.setContentType("application/json");
//			response.getWriter().write(json);
		}

	}

	/**
	 * 
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		MongoFactory mongoFactory = new MongoFactory();
		String loginString = request.getParameter("login");

		JsonParser parser = new JsonParser();
		JsonElement jArray = parser.parse(loginString).getAsJsonObject();
		Login login = new Gson().fromJson(jArray, Login.class);

		BasicDBObject obj = login.setDbObject();
		mongoFactory.insertMongo(obj, "login");

	}

}
