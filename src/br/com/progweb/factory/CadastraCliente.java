package br.com.progweb.factory;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.progweb.objects.Cliente;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.mongodb.BasicDBObject;

public class CadastraCliente extends HttpServlet {

	MongoFactory mongoFactory = new MongoFactory();
	Mapper mapper = new Mapper();
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CadastraCliente() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		BasicDBObject query = new BasicDBObject();
		query.put("detail.nome", "Matheus");
		List<Cliente> listResponseBD = mapper.clienteMapper(mongoFactory.searchMongoNotComplete(null, "cliente"));
		if (listResponseBD.size() == 0) {
			response.getWriter().write("");
		} else {

			String json = new Gson().toJson(listResponseBD);
			response.setContentType("application/json");
			response.getWriter().write(json);
		}

	}

	/**
	 * 
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		MongoFactory mongoFactory = new MongoFactory();
		String clienteString = request.getParameter("cliente");

		JsonParser parser = new JsonParser();
		JsonElement jArray = parser.parse(clienteString).getAsJsonObject();
		Cliente cliente = new Gson().fromJson(jArray, Cliente.class);
		cliente.setIdCliente();

		BasicDBObject obj = cliente.setDbObject();
		mongoFactory.insertMongo(obj, "cliente");

	}
}
