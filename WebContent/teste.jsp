<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Teste</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.10/angular.min.js"></script>
<script>
var myApp = angular.module('myApp', []);

function MyController($scope, $http) {

        $scope.getDataFromServer = function() {
                $http({
                        method : 'GET',
                        url : '/br.com.progweb/CadastraCliente'
                }).success(function(data, status, headers, config) {
                        $scope.cliente = data;
                }).error(function(data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                });

        };
};
</script>
</head>
<body>
<div ng-app="myApp">
        <div ng-controller="MyController">
           <button ng-click="getDataFromServer()">Fetch data from server</button>
           <p>First Name : {{cliente.nome}}</p>
        </div>
</div>
</body>
</html>